﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Baseclass.Contrib.SpecFlow.Selenium.NUnit.Bindings;
using OpenQA.Selenium;

namespace AutomationTests
{
    public abstract partial class BasePage
    {
        public static string BaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["seleniumBaseUrl"];
            }
        }

        public static LoginPage LoadLoginPage(IWebDriver driver, string baseURL)
        {
            if (driver == null)
                driver = Browser.Current;
            driver.Navigate().GoToUrl(baseURL.TrimEnd(new char[] { '/' }) + LoginPage.URL);
            return GetInstance<LoginPage>(driver, baseURL, "");
        }
    }
}
