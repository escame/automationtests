﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomationTests
{
    public class LoginPage : BasePage
    {
        public static string URL = "/pants/#!/login";
        public override string DefaultTitle { get { return "NextGen"; } }

        [FindsBy(How = How.Id, Using = "btnSignin")]
        public IWebElement LoginButton;

        internal void IsLoginButtonAvailable()
        {
            if (LoginButton == null)
                throw new Exception("Unable to find the Login button on the page");

            AssertElementText(LoginButton, "Sign in", "login button");
        }

        [FindsBy(How = How.Id, Using = "userName")]
        public IWebElement UserNameTextBox;
        internal void PopulateUserNameTextBox(string userName)
        {
            UserNameTextBox.SendKeys(userName);
        }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement PasswordTextBox;
        internal void PopulatePasswordTextBox(string password)
        {
            PasswordTextBox.SendKeys(password);
        }

        internal BasePage ClickLoginButton()
        {
            LoginButton.Click();
            return GetInstance<DashboardPage>(Driver);
        }
    }
}
