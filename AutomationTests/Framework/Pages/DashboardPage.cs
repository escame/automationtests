﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomationTests
{
    public class DashboardPage : BasePage
    {
        public override string DefaultTitle { get { return "NextGen"; } }
    }
}
