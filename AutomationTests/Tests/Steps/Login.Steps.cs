﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace AutomationTests
{
    [Binding]
    public class Login : StepsBase
    {
        [Given(@"I am on the login page")]
        public void GivenIAmOnTheLoginPage()
        {
            CurrentPage = (BasePage)BasePage.LoadLoginPage(CurrentDriver, BasePage.BaseUrl);
        }

        [Given(@"I am not logged in")]
        public void GivenIAmNotLoggedIn()
        {
            CurrentPage.As<LoginPage>().IsLoginButtonAvailable();
        }

        [When(@"I type the user name and password with the following details")]
        public void WhenITypeTheUserNameAndPasswordWithTheFollowingDetails(dynamic form)
        {
            CurrentPage.As<LoginPage>().PopulateUserNameTextBox(form.UserName);
            CurrentPage.As<LoginPage>().PopulatePasswordTextBox(form.Password);
        }

        [When(@"I click the login button")]
        public void WhenIClickTheLoginButton()
        {
            NextPage = CurrentPage.As<LoginPage>().ClickLoginButton();
        }

        [Then(@"I redirect to the dashboard page")]
        public void ThenIRedirectToTheDashboardPage()
        {
            CurrentPage.Is<DashboardPage>();
        }

        [Then(@"I will see the message (.*)")]
        public void ThenIWillSeeTheMessage(string message)
        {
            CurrentPage.IsTextOnPage(message);
        }
    }
}
