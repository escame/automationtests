﻿Feature: Login
	In order to access features from Core
	As a registered user
	I want to login into my account

@Browser:Firefox
@Browser:Chrome
@Browser:IE
@Browser:PhantomJS
Scenario: User signs up with valid credentials
	Given I am on the login page
	And I am not logged in
	When I type the user name and password with the following details
	| field    | value              |
	| UserName | qa1@harddollar.com |
	| Password | Password123@       |
	And I click the login button
	Then I redirect to the dashboard page

@Browser:RemoteWebDriver
Scenario: User signs up with invalid password
	Given I am on the login page
	And I am not logged in
	When I type the user name and password with the following details
	| field    | value              |
	| UserName | qa1@harddollar.com |
	| Password | Mypassword         |
	And I click the login button
	Then I will see the message You have entered an invalid username or password